//const DocumentReader = require("../../plugins/cordova-plugin-document-reader-api/www/DocumentReader");

//module.exports = {
var Regula = {
    init: function (message_callback, success_callback, error_callback) {
        window.resolveLocalFileSystemURL(
            cordova.file.applicationDirectory + "www/regula.license",
            function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                        var license = this.result;
                        DocumentReader.prepareDatabase(
                            "Full",
                            function (message) {
                                message_callback(message);
                                if (message.substring(0, 11) == "Downloading") {
                                } else {
                                    //document.getElementById("status").innerHTML = "Loading......";
                                    DocumentReader.initializeReader(
                                        license,
                                        function (message) {
                                            console.log(message);
                                            DocumentReader.setConfig({
                                                functionality: {
                                                    videoCaptureMotionControl: true,
                                                    showCaptureButton: true
                                                }, customization: {
                                                    showResultStatusMessages: true,
                                                    showStatusMessages: true
                                                }, processParams: {
                                                    multipageProcessing: false,
                                                    scenario: "DocType",
                                                    //doRfid: false,
                                                },
                                            }, function (m) { }, error_callback);
                                            success_callback();
                                        },
                                        error_callback
                                    );
                                }
                            },
                            error_callback);
                    }
                    reader.readAsArrayBuffer(file);
                });
            },
            error_callback
        );
    }
};