var app = new Framework7({
    // App root element
    root: '#app',
    // App Name
    name: 'Demo',
    // App id
    id: 'com.ozforensics.Demo',
    // Enable swipe panel
    panel: {
        swipe: 'left',
    },
    // Add default routes
    routes: [{
            path: '/login',
            url: 'login.html'
        },
        {
            path: '/main',
            url: 'main.html'
        },
        {
            path: '/status',
            url: 'status.html'
        }
    ],
    // ... other parameters
});

var mainView = app.views.create('.view-main');
var sharedPreferences;
var actual_status = {
    'class': '',
    'message': '',
    'result': '',
}
var regulaInited = false;

document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    //sharedPreferences = SharedPreferences('settings');
    OzSDK.configure("https://api.oz-services.ru/", function(e) {
        console.log('Plugin is configured');
        /*sharedPreferences.get("TOKEN", function(token) {
            OzSDK.setToken(token, function() {
                mainView.router.navigate('/main');
            })
        }, function(error){*/
            mainView.router.navigate('/login');
        //});
    });
}

var $$ = Dom7;

$$(document).on('page:init page:reinit', '.page[data-name="login"]', function (e, page) {
    //sharedPreferences.del("TOKEN", function() {}, function() {});

    $$('.login_page_lang input').change(function () {
        if (this.value == 'en') {
            $$('.login_page_lang input')[1].checked = false;
        } else {
            $$('.login_page_lang input')[0].checked = false;
        }
    });

    $$('button.login_submit_button').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        let login = $$('[name=login]').val();
        let password = $$('[name=password]').val();
        OzSDK.login({
            login: login,
            password: password
        }, function(token) {
            //sharedPreferences.put("TOKEN", token, function() {
                mainView.router.navigate('/main');
            //}, function () {});
        }, function(error) {
            console.error(error);
        })
    });
});

$$(document).on('page:init page:reinit', '.page[data-name="main"]', function (e, page) {
    $$('.action_menu_wrap_item:nth-child(2) .action_menu_item').click(function () {
        let $sub = $$('.action_submenu');
        $sub.css('display', $sub.css('display') === 'block' ? 'none' : 'block');
    });

    $$('.demo_page_close').click(function() {
        OzSDK.logout(function() {
            mainView.router.back();
        });
    });

    $$('.action_submenu_item').click(function (e) {
        let data = $$(this).data('action');
        OzSDK.record(data, function(media) {
            actual_status.class = '_processing';
            actual_status.message = 'Uploading...';
            mainView.router.navigate('/status');
            upload_media(media);
        }, function(errors) {
            console.log(errors);
        });
    });

    $$('#onboarding').click(onboarding);
});

function update_status() {
    $$(".result_page_title").text(actual_status.message);
    $$(".result_icon").attr('class', 'result_icon '+actual_status.class);
    $$("#result").text(actual_status.result);
}

function setFailedStatus(error, message = null) {
    actual_status.class = '_failed';
    if (message) actual_status.message = 'Upload failed';
    actual_status.result = error;
    update_status();
}

function upload_media(media) {
    OzSDK.upload(media, function(results) {
        actual_status.message = 'Upload complete'
        actual_status.result = 'analysing...';
        update_status();
        analyze_media(results);
    }, setFailedStatus);
}

function analyze_media(response) {
    OzSDK.analyze(response.folder_id, function(results) {
        actual_status.class = '_success';
        actual_status.message = 'Analyze done';
        actual_status.result = JSON.stringify(results);
        update_status();
        console.log(JSON.stringify(results));
    }, setFailedStatus)
}

$$(document).on('page:init page:reinit', '.page[data-name="status"]', function (e, page) {
    update_status();
    $$(".custom_button").click(function() {
        mainView.router.back();
    });
});

function onboarding() {
    if (!regulaInited) {
        actual_status.class = "_processing";
        actual_status.message = "Regula init...";
        actual_status.result = "";
        mainView.router.navigate('/status');
        Regula.init(function(message) {
            actual_status.result = message;
            update_status();
        }, function() {
            regulaInited = true;
            showScanner();
        }, setFailedStatus);
    } else {
        showScanner();
    }
}

function showScanner() {
    DocumentReader.showScanner(function(results) {
        actual_status.result = "Document scanned";
        update_status();
        scanSelfie(results);
    }, setFailedStatus);
}

function scanSelfie(docResults) {
    actual_status.message = "Record selfie...";
    update_status();
    let gests = OzSDK.getABConfig("actions") || "video_selfie_blank";
    OzSDK.record(gests, function(media) {
        actual_status.result = "Selfie scanned";
        update_status();
        uploadOnboarding(docResults, media);
    }, setFailedStatus);
}

function uploadOnboarding(docResults, media) {
    actual_status.message = "Uploading and analyzing...";
    update_status();
    OzSDK.saveDocumentResults(docResults, function(medias) {
        actual_status.result = "Documents saved";
        update_status();
        let all_medias = medias.concat(media);
        OzSDK.uploadFolder(all_medias, function(results) {
            actual_status.message = "Completed"
            actual_status.result = `FOLDER ID ${results[0].folder_id} \n`;
            for (let result of results) {
                actual_status.result += `${result.type}: ${result.resolution} \n`;
            }
            actual_status.class = "_success";
            update_status();
        }, setFailedStatus);
    }, setFailedStatus);
}