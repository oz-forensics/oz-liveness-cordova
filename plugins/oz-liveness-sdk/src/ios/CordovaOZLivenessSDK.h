#import <Cordova/CDVPlugin.h>
#import <Foundation/Foundation.h>
#import <OZLivenessSDK/OZLivenessSDK-Swift.h>

@class OZLivenessSDKcls;

@interface CordovaOZLivenessSDK : CDVPlugin

@property (nonatomic, strong) CordovaOZLivenessSDK *cordovaOZLivenessSDK;

@end
