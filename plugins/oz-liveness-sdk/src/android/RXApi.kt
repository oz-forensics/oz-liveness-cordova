package com.ozforensics.liveness.cordova

import android.content.Context
import com.ozforensics.liveness.sdk.core.model.OzAnalysisResult
import com.ozforensics.liveness.sdk.core.model.OzMedia
import com.ozforensics.liveness.sdk.api.OzForensicsService
import com.ozforensics.liveness.sdk.api.model.*
import com.ozforensics.liveness.sdk.core.OzLivenessSDK
import com.ozforensics.liveness.sdk.core.StatusListener
import com.ozforensics.liveness.sdk.core.exceptions.OzException
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.Subject
import java.io.File
import java.util.concurrent.TimeUnit

object RXApi {
    private var service: OzForensicsService? = null

    fun recreateService(context: Context, baseUrl: String, token: String) {
        service = OzForensicsService(context, baseUrl, token)
    }

    fun uploadMediaAndAnalyze(
        context: Context,
        mediaList: List<OzMedia>,
        statusSubject: Subject<String>?
    ): Single<List<OzAnalysisResult>> =
        Single.create {
            val listener = object : StatusListener<List<OzAnalysisResult>> {
                override fun onStatusChanged(status: String?) {
                    status?.let { statusSubject?.onNext(it) }
                }

                override fun onSuccess(result: List<OzAnalysisResult>) {
                    it.onSuccess(result)
                }

                override fun onError(error: OzException) {
                    if (!it.isDisposed) it.onError(error)
                }
            }
            val cancelable = OzLivenessSDK.uploadMediaAndAnalyze(context, mediaList, listener)
            it.setCancellable {
                cancelable?.cancel()
            }
        }

    fun uploadPersonalizationVideo(video: OzMedia): Single<Pair<String, String>> {
        return Single.create { emitter ->
            val file = File(video.filePath)
            val cancelable = service?.uploadPersonalizationVideo(
                file.readBytes(), file.name, file.name, video.tag!!,
                object : StatusListener<FoldersResponse> {
                    override fun onSuccess(result: FoldersResponse) {
                        result.let {
                            emitter.onSuccess(it.folderId!! to it.media[0].mediaId!!)
                        }
                    }

                    override fun onError(error: OzException) {
                        if (!emitter.isDisposed) emitter.onError(error)
                    }
                }
            )
            emitter.setCancellable {
                cancelable?.cancel()
            }
        }
    }

    fun uploadMediaToFolder(folderId: String, media: OzMedia): Single<Media> {
        return Single.create { emitter ->
            val file = File(media.filePath)
            val cancelable = service?.uploadMediaToFolderWithSpecificId(
                media.type == OzMedia.Type.VIDEO,
                file.readBytes(),
                file.name,
                file.name,
                folderId,
                media.tag!!,
                object : StatusListener<List<Media>> {
                    override fun onSuccess(result: List<Media>) {
                        emitter.onSuccess(result[0])
                    }

                    override fun onError(error: OzException) {
                        if (!emitter.isDisposed) emitter.onError(error)
                    }
                }
            )
            emitter.setCancellable {
                cancelable?.cancel()
            }
        }
    }

    fun uploadMediaToNewFolder(media: OzMedia): Single<FoldersResponse> {
        return Single.create { emitter ->
            val file = File(media.filePath)
            val cancelable = service?.uploadMediaToNewFolder(
                media.type == OzMedia.Type.VIDEO,
                file.readBytes(),
                file.name,
                file.name,
                media.tag!!,
                object : StatusListener<FoldersResponse> {
                    override fun onSuccess(result: FoldersResponse) {
                        emitter.onSuccess(result)
                    }

                    override fun onError(error: OzException) {
                        if (!emitter.isDisposed) emitter.onError(error)
                    }
                }
            )
            emitter.setCancellable {
                cancelable?.cancel()
            }
        }
    }

    fun analyzePersonalizationVideo(folderId: String, videoId: String): Single<List<AnalysesResponse>> {
        val analysesRequest = AnalysesRequest(
            listOf(
                AnalysesRequest.AnalyseRequest(
                    "quality",
                    null,
                    listOf(videoId)
                )
            )
        )
        return analyze(folderId, analysesRequest)
    }

    fun analyze(folderId: String, analysisRequest: AnalysesRequest): Single<List<AnalysesResponse>> {
        return addFolderAnalysisRequest(folderId, analysisRequest)
            .map { it.first }
            .flatMap { getProcessedAnalysis(it) }
    }

    fun addFolderAnalysisRequest(
        folderId: String,
        analysisRequest: AnalysesRequest
    ): Single<Pair<String, List<AnalysesResponse>>> {
        return Single.create { emitter ->
            val cancelable = service?.addFolderAnalyzes(
                folderId,
                analysisRequest,
                object : StatusListener<List<AnalysesResponse>> {
                    override fun onSuccess(result: List<AnalysesResponse>) {
                        emitter.onSuccess(folderId to result)
                    }

                    override fun onError(error: OzException) {
                        if (!emitter.isDisposed) emitter.onError(error)
                    }
                }
            )
            emitter.setCancellable {
                cancelable?.cancel()
            }
        }
    }

    private fun listAnalysisRequest(folderId: String): Single<List<AnalysesResponse>> {
        return Single.create { emitter ->
            val cancelable = service?.getAnalyzesForFolder(
                folderId,
                object : StatusListener<List<AnalysesResponse>> {
                    override fun onSuccess(result: List<AnalysesResponse>) {
                        emitter.onSuccess(result)
                    }

                    override fun onError(error: OzException) {
                        if (!emitter.isDisposed) emitter.onError(error)
                    }
                }
            )
            emitter.setCancellable {
                cancelable?.cancel()
            }
        }
    }

    fun getProcessedAnalysis(folderId: String): Single<List<AnalysesResponse>> {
        return listAnalysisRequest(folderId)
            .repeat()
            .skipWhile { it.any { it.state == "PROCESSING" } }
            .firstOrError()
    }

    fun createFolderSingle(): Single<String> {
        return Single.create { emitter ->
            val cancelable = service?.addFolder(
                object : StatusListener<AddFolderResponse> {
                    override fun onSuccess(result: AddFolderResponse) {
                        emitter.onSuccess(result.folderId)
                    }

                    override fun onError(error: OzException) {
                        if (!emitter.isDisposed) emitter.onError(error)
                    }
                }
            )
            emitter.setCancellable {
                cancelable?.cancel()
            }
        }
    }
}
