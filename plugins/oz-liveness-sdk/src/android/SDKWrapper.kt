package com.ozforensics.liveness.cordova

import android.content.Intent
import android.os.Build
import android.util.Base64
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import org.apache.cordova.CallbackContext
import org.apache.cordova.CordovaPlugin
import org.json.JSONArray
import org.json.JSONException
import com.ozforensics.liveness.sdk.core.OzLivenessSDK
import com.ozforensics.liveness.sdk.core.StatusListener
import com.ozforensics.liveness.sdk.core.exceptions.OzException
import com.ozforensics.liveness.sdk.core.model.OzMedia
import com.ozforensics.liveness.sdk.core.model.OzMediaTag
import com.ozforensics.liveness.sdk.api.model.AnalysesRequest
import com.ozforensics.liveness.sdk.api.model.Media
import com.ozforensics.liveness.sdk.logging.JournalObserver
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import org.apache.cordova.CordovaInterface
import org.apache.cordova.CordovaWebView
import org.json.JSONObject
import java.io.File


class SDKWrapper: CordovaPlugin() {
	lateinit var context: CallbackContext
    lateinit var serverUrl: String
    private var uploadDisposable: Disposable? = null
    private var analyzeDisposable: Disposable? = null

    private val REQUEST_PERSONALISATION = 1000


    /**
     * Executes the request.
     *
     * @param action        	The action to execute.
     * @param args          	JSONArry of arguments for the plugin.
     * @param callbackContext 	The callback context used when calling back into JavaScript.
     * @return              	True if the action was valid, false if not.
     */
	@Throws(JSONException::class)
    override fun execute(action: String, data: JSONArray, callbackContext: CallbackContext): Boolean {
        context = callbackContext
        var result = true
        try {
            if (action == "configure") {
                serverUrl = data.getString(0)
                OzLivenessSDK.logging.journalObserver = object: JournalObserver {
                    override fun update(event: String) {
                        cordova.activity.runOnUiThread {
                            webView.loadUrl("javascript:console.log('${event.replace("'", "''")}')")
                        }
                    }
                }
                OzLivenessSDK.configure(webView.context, serverUrl)
                OzLivenessSDK.log(
                    webView.context,
                    TAG,
                    "App started. SDK version is ${OzLivenessSDK.version}"
                )
                callbackContext.success()
            } else if (action == "setToken") {
                val token = data.getString(0)
                RXApi.recreateService(webView.context, serverUrl, token)
                callbackContext.success()
            } else if (action == "login") {
                val options = data.getJSONObject(0)
                val login = options.getString("login")
                val password = options.getString("password")
                val listener = object : StatusListener<String> {
                    override fun onSuccess(result: String) {
                        RXApi.recreateService(webView.context, serverUrl, result)
                        context.success("Login success")
                    }
                    override fun onError(error: OzException) {
                        context.error(error.toString())
                    }
                }
                OzLivenessSDK.login(webView.context, login, password, listener)
                return true
            } else if (action == "logout") {
                OzLivenessSDK.logOut(webView.context)
                callbackContext.success()
            } else if (action == "record") {
                val gest_list = (0 until data.length()).map { data.getString(it) }
                    .map { OzLivenessSDK.OzAction.fromOzMediaTagString(it) } .filterNotNull() .toList()
                val intent = OzLivenessSDK.createStartIntent(cordova.activity, gest_list)
                cordova.setActivityResultCallback(this)
                cordova.startActivityForResult( this, intent, REQUEST_PERSONALISATION)
            } else if (action == "upload") {
                val jdata = data.getJSONArray(0).getJSONObject(0)
                val media = Gson().fromJson(jdata.toString(), OzMedia::class.java)
                uploadVideo(media)
            } else if (action == "analyze") {
                val folderId = data.getString(0)
                analyze(folderId)
            } else if (action == "getABConfig") {
                val key = data.getString(0)
                val res = OzLivenessSDK.getABConfigForKey(key)
                context.success(JSONArray(res))
            } else if (action == "saveDocumentResults") {
                val g_list = JSONObject(data.getString(0)).getJSONObject("graphicResult").getJSONArray("fields")
                val files_list = (0 until g_list.length()).map { g_list.getJSONObject(it)
                    .let {Pair(it.getString("value"), it.getInt("fieldType"))} }
                    .filter { it.second in arrayOf(201, 207) }
                    .map { saveEncodedFile(it.first, it.second) }
                    .map { OzMedia(OzMedia.Type.PHOTO, it.first, if (it.second==201) OzMediaTag.PhotoIdFront else OzMediaTag.PhotoIdBack) }
                    .toList()
                if (files_list.count() != 2)
                    context.error("Wrong document's page")
                else
                    context.success(JSONArray(Gson().toJson(files_list)))
            } else if (action == "uploadFolder") {
                val media_list = (0 until data.length()).map { data.getJSONObject(it) }
                    .map { Gson().fromJson(it.toString(), OzMedia::class.java)}
                    .filterNotNull() .toList()
                uploadFolder(media_list)
            } else {
                handleError("Invalid action")
                result = false
            }
        } catch (e: Exception) {
            handleException(e)
            result = false
        }

        return result
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        if (intent == null) return
        if (requestCode == REQUEST_PERSONALISATION) {
            OzLivenessSDK.log(webView.context, TAG, "Gest detection code: ${resultCode}")
            val results = OzLivenessSDK.getResultFromIntent(intent)
            val error = OzLivenessSDK.getErrorFromIntent(intent)
            if (error != null)
                context.error(error.toString())
            else if (results == null)
                context.error("action closed")
            else
                context.success(JSONArray(Gson().toJson(results)))
        }
    }

    private fun uploadVideo(video: OzMedia) {
        uploadDisposable?.dispose()
        uploadDisposable = RXApi.uploadPersonalizationVideo(video)
            .subscribeOn(Schedulers.io())
            .flatMap { RXApi.analyzePersonalizationVideo(it.first, it.second) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { res, error ->
                if (error != null) {
                    context.error(error.toString())
                } else if (res != null) {
                    context.success(JSONObject(Gson().toJson(res[0])))
                }
            }
    }

    private fun analyze(folderId: String) {
        analyzeDisposable?.dispose()
        analyzeDisposable = RXApi.getProcessedAnalysis(folderId)
            .subscribeOn(Schedulers.io())
            .subscribe { res, error ->
                if (error != null) {
                    context.error(error.toString())
                } else if (res != null) {
                    val analyses = res.take(2)
                    context.success(JSONArray(Gson().toJson(analyses)))
                }
            }
    }

    private fun saveEncodedFile(data: String, fieldType: Int):Pair<String,Int> {
        val bytes = Base64.decode(data, Base64.DEFAULT)
        return File(webView.context.filesDir, "images").let { parent ->
            parent.mkdirs()
            File(parent, "doc${fieldType}.jpg")
                .let {
                    it.writeBytes(bytes)
                    Pair(it.absolutePath, fieldType)
                }
        }
    }

    private fun uploadFolder(mediaSource: List<OzMedia>) {
        uploadDisposable?.dispose()
        uploadDisposable = RXApi.createFolderSingle()
            .flatMap {
                Single.zip(
                    Single.just(it),
                    //mediaSource.subscribeOn(Schedulers.io()),
                    Single.just(mediaSource),
                    BiFunction { t1: String, t2: List<OzMedia> -> t1 to t2 }
                )
            }
            .flatMap { (folderId, medias) ->
                Single.zip(
                    Observable.fromIterable(medias)
                        .flatMap { media -> RXApi.uploadMediaToFolder(folderId, media).toObservable() }
                        .toList(),
                    Single.just(folderId), BiFunction { f: List<Media>, s: String -> f to s })
            }
            .subscribeOn(Schedulers.io())
            .subscribe { medias, error ->
                if (error != null)
                    context.error(error.toString())
                else
                    analyzeOnboarding(medias.first, medias.second)
            }
    }

    private fun analyzeOnboarding(medias: List<Media>, folderId: String) {
        val documentIds = medias.filter { it.mediaType == "IMAGE_FOLDER" }
        val videoIds = medias.filter { it.mediaType == "VIDEO_FOLDER" }

        val qualityAnalysis = AnalysesRequest.AnalyseRequest("quality", null, videoIds.map { it.mediaId!! })

        val params = listOf("capabilities" to 492).toMap()
        val documentsAnalysis =
            AnalysesRequest.AnalyseRequest("documents", null, documentIds.map { it.mediaId!! }, params)

        val toBiometry = (videoIds + documentIds)
            .filter { it.tags?.contains(OzMediaTag.PhotoIdBack.value) != true }
            .map { it.mediaId!! }
        val biometryAnalysis = AnalysesRequest.AnalyseRequest("biometry", null, toBiometry)
        val composed = AnalysesRequest(
            listOf(
                qualityAnalysis,
                documentsAnalysis,
                biometryAnalysis
            )
        )

        analyzeDisposable?.dispose()
        analyzeDisposable = RXApi.analyze(folderId, composed)
            .subscribeOn(Schedulers.io())
            .subscribe { res, error ->
                if (error != null) {
                    context.error(error.toString())
                } else if (res != null) {
                    context.success(JSONArray(Gson().toJson(res)))
                }
            }
    }

    /**
     * Handles an error while executing a plugin API method.
     * Calls the registered Javascript plugin error handler callback.
     *
     * @param errorMsg Error message to pass to the JS error handler
     */
    private fun handleError(errorMsg: String) {
        try {
            Log.e(TAG, errorMsg)
            context.error(errorMsg)
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }

    }

    private fun handleException(exception: Exception) {
        handleError(exception.toString())
    }

    companion object {
        protected val TAG = "OZSDK"
    }
}