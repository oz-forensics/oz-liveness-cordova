var exec = require('cordova/exec');

module.exports = {
    configure: function (service_url, callback) {
		exec(callback, callback, "OzSDK", "configure", [service_url]);
	},
	/* Auth function
	auth_data:
	{
		login: "...",
		password: "..."
	}
	*/
	login: function(auth_data, callback, error_callback) {
		exec(callback, error_callback, "OzSDK", "login", [auth_data]);
	},
	logout: function(callback) {
		exec(callback, callback, "OzSDK", "logout", [0]);
	},
	setToken: function(token, callback) {
		exec(callback, callback, "OzSDK", "setToken", [token]);
	},
	/* Function for local video recording
	Gests contains one element or list of gest tokens:
		video_selfie_blank    video_selfie_best      video_selfie_smile  video_selfie_eyes
		video_selfie_zoom_in  video_selfie_zoom_out  video_selfie_scan
		video_selfie_high     video_selfie_down      video_selfie_left   video_selfie_right
	Result has structure (OzMedia):
	{
		filePath: "...", // local file with video chunk
		tag: "...",
		type: "VIDEO"
	}
	*/
	record: function(gests, callback, error_callback) {
		exec(callback, error_callback, "OzSDK", "record", Array.isArray(gests)?options:[gests]);
	},
	/* Function to upload video chunk
	Media parameter contains media data from "record" request.
	Result has structure (AnalysesResponse):
	{
		analyse_id: "aba...",
		folder_id: "2ec...",
		resolution: "SUCCESS",
		resolution_status: "SUCCESS",
		results_media: [{
			analyse_id: "aba...",
			media_association_id: 172020,
			media_association_type: "VIDEO"
		}, ...],
		source_media: [{
			info: {
				preview: {
					FPS: 29.97...,
					duration: 0.6,
					height: 720,
					md5: "f1a...",
					"mime-type": "video/mp4",
					size: 1175782,
					width: 960
				},
				thumb: {
					height: 300,
					md5: "0c7...",
					"mime-type": "image/jpeg",
					size: 13338,
					width: 225
				}
			},
			media_id: "de8...",
			media_type: "VIDEO_FOLDER",
			original_name: "bde...037.mp4",
			thumb_url: "https://api.oz-services.ru:8443/static/2ec...f9b/de8...d4a_thumb.jpg",
			time_created: 1.60...,
			time_updated: 1.60...
		}, ...],
		state: "FINISHED",
		time_created: 1.60...,
		time_updated: 1.60...,
		type:"QUALITY"
	}
	*/
	upload: function(media, callback, error_callback) {
		exec(callback, error_callback, "OzSDK", "upload", [media]);
	},
	analyze: function(folder_id, callback, error_callback) {
		exec(callback, error_callback, "OzSDK", "analyze", [folder_id]);
	},
	getABConfig: function(key, callback) {
		exec(callback, callback, "OzSDK", "getABConfig", [key]);
	},
	uploadFolder: function(medias, callback, error_callback) {
		exec(callback, error_callback, "OzSDK", "uploadFolder", medias);
	},
	saveDocumentResults: function(documentResults, callback, error_callback) {
		exec(callback, error_callback, "OzSDK", "saveDocumentResults", [documentResults]);
	}
}